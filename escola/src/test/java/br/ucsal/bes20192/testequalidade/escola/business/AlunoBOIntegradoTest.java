package br.ucsal.bes20192.testequalidade.escola.business;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOIntegradoTest {

	private AlunoDAO dao = new AlunoDAO();
	private AlunoBO alunoBO = new AlunoBO(dao, new DateHelper());

	//Antes de rodar o teste estamos salvando no banco o aluno padr�o de teste
	@Before
	public void setUp() {
		AlunoBuilder builder = AlunoBuilder.criar().comMatricula(10).comNome("Cajuzinho").comSituacaoAtivo()
				.comAnoNascimento(2002);
		Aluno aluno = builder.build(builder);
		dao.salvar(aluno);
		
	}

	@Test
	public void testarCalculoIdadeAluno1() {

		// PODE DAR ERRO SE N�O TIVER CONEX�O COM O BANCO

		Integer idadeEsperada = 16;
		Integer result = alunoBO.calcularIdade(10);
		assertEquals(idadeEsperada, result);
	}

	@Test
	public void testarAtualizacaoAlunosAtivos() {

		// PODE DAR ERRO SE N�O TIVER CONEX�O COM O BANCO

		Aluno aluno = dao.encontrarPorMatricula(10);
		alunoBO.atualizar(aluno);

	}

}
