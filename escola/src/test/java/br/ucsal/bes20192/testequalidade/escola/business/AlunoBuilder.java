package br.ucsal.bes20192.testequalidade.escola.business;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;

public class AlunoBuilder {
	private Integer matricula;
	private String nome;
	private SituacaoAluno situacao;
	private Integer anoNascimento;
 
    public static AlunoBuilder criar() {
        return new AlunoBuilder();
    }
    public AlunoBuilder comMatricula(Integer matricula) {
        this.matricula = matricula;
        return this;
    }
    public AlunoBuilder comNome(String nome) {
        this.nome = nome;
        return this;
    }
     
    public AlunoBuilder comSituacaoCancelado() {
        this.situacao = SituacaoAluno.CANCELADO;
        return this;
    }
    public AlunoBuilder comSituacaoAtivo() {
        this.situacao = SituacaoAluno.ATIVO;
        return this;
    }
    public AlunoBuilder comAnoNascimento(Integer anoNascimento) {
        this.anoNascimento = anoNascimento;
        return this;
    }
 
    public Aluno build(AlunoBuilder build) {
        Aluno aluno = new Aluno();
        aluno.setMatricula(build.matricula);
        aluno.setNome(build.nome);
        aluno.setSituacao(build.situacao);
        aluno.setAnoNascimento(build.anoNascimento);
         
		return aluno;
    }
}

